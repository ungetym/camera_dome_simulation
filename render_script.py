import bpy
from math import pi, cos, sin, tan

################################# Configuration Data ####################################

##### Camera data #####

# Number of cameras 
num_of_cams = 20
# Distance between neighboring cameras in m
distance_between_cams_m = 0.5
# Distance of a camera from the floor in m
distance_cam_to_floor_m = 1.8
# Tilt angle towards the floor - 0 means camera viewing direction is parallel to floor
tilt_towards_floor_degree = 13 
# Horizontal field of view in degrees
FoV_degrees = 60
# Camera resolution and pixel size in um
resolution_x = 1440
resolution_y = 1080
pixel_size_um = 3.45


##### Other data #####

# Number of samples per pixel during rendering
num_of_samples = 64 
# Output dir for saving the renderings
output_dir = '/home/anonym/Renderings/'
# Image file name - this will be extended by the camera index, so here the filenames will
# be example_0.jpg, example_1.jpg, ...
image_name = 'example'
# Image file type, default is 'JPEG', but can e.g. also be 'PNG' or 'BMP'
file_type = 'JPEG'
# Enable/disable depth of field. If enabled, the focus distance is automatically 
# calculated to be the center of the half circle the cameras are located on
enable_DoF = True
# Aperture setting if DoF is enabled
f_stop = 1.8

################################ General renderer setup #################################

# Set render engine
bpy.data.scenes["Scene"].render.engine = 'CYCLES'
# Set GPU rendering if supported
bpy.data.scenes["Scene"].cycles.device = 'GPU'
# Enable/disable denoising
bpy.data.scenes["Scene"].cycles.use_denoising = False
# Set number of samples
bpy.data.scenes["Scene"].cycles.samples = num_of_samples
# Set output file type
bpy.data.scenes["Scene"].render.image_settings.file_format = file_type


############################## Create Dome Configuration ################################

# Calculate radius (in m) of half circle the cameras are located on
radius_m =  (2 * num_of_cams * distance_between_cams_m) / (2 * pi)

# Place all 20 cameras on a half circle with the calculated radius facing the circle's center
cam_positions_x = []
cam_positions_y = []
cam_rotations = []
angle_step = pi/(num_of_cams -1)
for i in range(0,num_of_cams):
    cam_positions_x.append(radius_m * cos(-i * angle_step))
    cam_positions_y.append(radius_m * sin(-i * angle_step))
    cam_rotations.append(pi/2 - i * angle_step)
    
    
##################################### Camera Setup ######################################
    
# Set resolution
bpy.data.scenes["Scene"].render.resolution_x = resolution_x
bpy.data.scenes["Scene"].render.resolution_y = resolution_y
bpy.data.cameras["Camera"].clip_start = 0.001
# Set sensor
bpy.data.cameras["Camera"].sensor_fit = 'HORIZONTAL'
bpy.data.cameras["Camera"].sensor_width = resolution_x * pixel_size_um / 1000
bpy.data.cameras["Camera"].sensor_height = resolution_y * pixel_size_um / 1000
# Set focal length
bpy.data.cameras["Camera"].lens = bpy.data.cameras["Camera"].sensor_width / (2 * tan(FoV_degrees * pi / 360))
# Set DoF properties
bpy.data.cameras["Camera"].dof.use_dof = enable_DoF
if enable_DoF:
    bpy.data.cameras["Camera"].dof.focus_distance = radius_m
    bpy.data.cameras["Camera"].dof.aperture_fstop = f_stop
    bpy.data.cameras["Camera"].dof.aperture_blades = 8

################################### Rendering Loop ######################################

# int to string function for filenames
def STRi(value):
    return "{:.0f}".format(value)

for i in range(0, num_of_cams):
    # Set camera pose
    bpy.data.objects["Camera"].rotation_euler[0] = pi/2 - tilt_towards_floor_degree /180 * pi
    bpy.data.objects["Camera"].rotation_euler[2] = cam_rotations[i]
    bpy.data.objects["Camera"].location[0] = cam_positions_x[i]
    bpy.data.objects["Camera"].location[1] = cam_positions_y[i]
    bpy.data.objects["Camera"].location[2] = distance_cam_to_floor_m
    # Generate image file name and path
    image_file_name = image_name+'_'+STRi(i)+'.png'
    bpy.data.scenes["Scene"].render.filepath = output_dir + image_file_name
    # Start rendering
    bpy.ops.render.render(write_still=True)