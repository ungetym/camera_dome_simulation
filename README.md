# Camera_Dome_Simulation

A simple scene and render script for Blender to simulate a number of cameras located on a half circle around a focus point.

#### Usage

Open the .blend file with [Blender >= 3.3.1](https://www.blender.org/download/)

The Blender window should now look similar to this screenshot:

![Screenshot](https://gitlab.com/ungetym/camera_dome_simulation/-/raw/main/Doc/0_initial.jpg)

In order to get a preview, set the viewport shading of the 3D view to 'Rendered' (red marker). Make also sure to use the Cycles renderer and if available, enable GPU support (green marker). 

Set the camera configuration values as well as output path in the text editor tab and press play (blue marker). Depending on your hardware capabilities, the images should be rendered within a few minutes. Some exemplary renderings are available in the Renderings folder.

#### Notes

The camera image are perfect in the sense, that no distortions are present. Furthermore, the DoF is purely synthetic and can differ from real out-of-focus effects.

The statue model is public domain (CC0 license) and available [here](https://sketchfab.com/3d-models/ehrengrab-johannes-benk-d33712df3716461bba26a589711b8ec5).
